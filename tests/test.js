describe("vendingMaching", function(){
    var vm = vendingMachine;

    it("should be able to create the object", function(){
        expect(vm).toBeDefined();
    }); // should be able to create the object

    it("should create variables for tracking money", function(){
        expect(vm.coinTotal).toEqual(0);
        expect(vm.coinReturn).toEqual(0);
        expect(vm.changeDue).toEqual(0);
        expect(vm.cashDrawer).toEqual(0.50);
    }); // should create variables for tracking money 

    describe("update Display", function(){
         it("should be able to update display", function(){
            expect(vm.updateDisplay).toBeDefined();
        }); // should be able to update coinReturn
    });

    describe("add method", function(){
        
        it("should be able to update coinTotal", function(){
            expect(vm.add).toBeDefined();
        }); // should be able to update coinTotal

        it("should update coinTotal", function(){
            vm.add(.15);
            expect(vm.coinTotal).toEqual(0.15);
        });  // should update coinTotal

    }); // add method

    describe("reject method", function(){
        
        it("should be able to update coinReturn", function(){
            expect(vm.reject).toBeDefined();
        }); // should be able to update coinReturn

        it("should update coinReturn", function(){
            vm.reject(.05);
            expect(vm.coinReturn).toEqual(0.05);
        });  // should update coinTotal

    }); // reject method

    describe("returnCoins method", function(){

        
        it("should be able to returnCoins", function(){
            expect(vm.returnCoins).toBeDefined();
        }); // should be able to update coinReturn

        it("should set coinTotal and coinReturn to zero", function(){
            vm.returnCoins(0);  
            expect(vm.coinReturn).toEqual(0);
            expect(vm.coinTotal).toEqual(0);
        }); // should set coinTotal and coinReturn to zero


    }); // returnCoins method

     describe("attemptPurchase method", function(){
        
        it("should be able to attemptPurchase", function(){
            expect(vm.attemptPurchase).toBeDefined();
        }); // should be able to attemptPurchase

    }); // attemptPurchase
});

